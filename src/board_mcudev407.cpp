#include "board.hpp"


#include <cortexpipi/systick.hpp>
#include <libopencm3/stm32/rcc.h>



namespace {
    void _initClock() {
        using Systick = cortexpipi::Systick;
        rcc_clock_setup_pll(&rcc_hse_8mhz_3v3[RCC_CLOCK_3V3_168MHZ]);
        auto& systick = Systick::instance();
        systick.init();
    }

    void _initRccPeriph() {
        rcc_periph_clock_enable(RCC_GPIOA);
        rcc_periph_clock_enable(RCC_GPIOE);
    }
}

Board::Board():
led(0,6),
btn(4,4)
{
    _initClock();
    _initRccPeriph();

    led.configure(GpioPin::Mode::OUT);
    btn.configure(GpioPin::Mode::IN);
}
