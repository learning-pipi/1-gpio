
#include "board.hpp"
#include <cortexpipi/systick.hpp>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>


using cortexpipi::Systick;


int main() {
    // Do not touch BEGIN
    auto& systick = cortexpipi::Systick::instance();
    auto& board = Board::instance();
    // Do not touch END

    const uint32_t led2_port = GPIOA;
    const uint32_t led2_pin = GPIO7;
    rcc_periph_clock_enable(RCC_GPIOA);
    gpio_mode_setup(led2_port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE,  led2_pin);
    gpio_set_output_options(led2_port, GPIO_OTYPE_PP, GPIO_OSPEED_25MHZ,  led2_pin);
    gpio_set(led2_port, GPIO7);



    for(;;) {
        gpio_toggle(GPIOA, GPIO7);
        // Do not touch BEGIN
        board.led.toggle();
        systick.delay(board.btn.read()?100:200);
        // Do not touch END
    }

    return 0;
}
