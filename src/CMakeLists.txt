



add_executable(
firmware.elf
main.cpp
)

add_dependencies(firmware.elf ${LIBCORTEXPIPI_LIBS})

target_link_libraries(
firmware.elf
${LIBCORTEXPIPI_LIBS}
)

target_include_directories(firmware.elf PRIVATE ${LIBCORTEXPIPI_INCLUDE})
target_compile_options(firmware.elf PRIVATE ${LIBCORTEXPIPI_COMPILE_FLAGS})

add_custom_target(
  firmware.bin
  # ALL
  BYPRODUCTS ${CMAKE_CURRENT_SOURCE_DIR}/firmware.bin
  DEPENDS firmware.elf
  COMMENT "generate .bin file"
  COMMAND ${OBJCOPY_BIN} -O binary ${CMAKE_CURRENT_BINARY_DIR}/firmware.elf
    ${CMAKE_CURRENT_BINARY_DIR}/firmware.bin
)

add_custom_target(
        firmware_size ALL
        DEPENDS firmware.elf
        COMMAND ${ARM_SIZE} ${CMAKE_CURRENT_BINARY_DIR}/firmware.elf
        COMMENT "display firmware size"
        WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
)

add_custom_target(
        flash
        DEPENDS firmware.bin
        COMMAND st-flash write ${CMAKE_CURRENT_BINARY_DIR}/firmware.bin 0x08000000
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "st-flash"
)

add_custom_target(
        flash-dfu
        DEPENDS firmware.bin
        COMMAND dfu-util -a 0 -s 0x08000000 -D ${CMAKE_CURRENT_BINARY_DIR}/firmware.bin
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "flash-dfu"
)

# TODO: autodetect BMP serial
add_custom_target(
      flash-bmp
      DEPENDS firmware.elf
      COMMAND
        arm-none-eabi-gdb -nx --batch
        -ex 'target extended-remote /dev/ttyACM0'
        -ex 'monitor swdp_scan'
        -ex 'attach 1'
        -ex 'load'
        -ex 'compare-sections'
        -ex 'kill'
        ${CMAKE_CURRENT_BINARY_DIR}/firmware.elf
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
      COMMENT "flash-bmp"
)

### Board impl select ###
set(BOARD_IMPL_SOURCE board_${BOARD}.cpp)
if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${BOARD_IMPL_SOURCE})
  message(FATAL_ERROR "Board ${BOARD} is not implemented. You should specify implemented one.")
else()
  target_sources(firmware.elf PRIVATE ${BOARD_IMPL_SOURCE})
endif()
